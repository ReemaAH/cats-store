FROM python:3.7-slim

RUN apt-get update && apt-get install

RUN apt-get install -y \
  dos2unix \
  libpq-dev \
  libmariadb-dev-compat \
  libmariadb-dev \
  gcc \
  && apt-get clean

RUN mkdir /code

WORKDIR /code 

COPY . /code/

RUN pip3 install virtualenv

RUN python -m venv env

RUN ./env/bin/pip install -r requirements.txt

RUN ["chmod", "+x", "./docker-entrypoint.sh"]
