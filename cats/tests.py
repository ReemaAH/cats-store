from django.test import TestCase

from .models import Cat


class AnimalTestCase(TestCase):
    def setUp(self):
        Cat.objects.create(
            name="Leo",
            age=1,
            category="British Shorthair")

    def test_cat_name(self):
        """Animals that can speak are correctly identified"""
        cat = Cat.objects.get(name="Leo")

        self.assertEqual(cat.info, 'Leo is 1 from British Shorthair')
