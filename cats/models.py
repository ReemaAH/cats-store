from django.db import models


class Cat(models.Model):
    name = models.CharField(blank=False, max_length=100)
    age = models.IntegerField(blank=False, default=0)
    category = models.CharField(blank=False, max_length=100)
    image = models.ImageField(upload_to='uploads/', null=True, blank=True)

    @property
    def info(self):
        return "{} is {} from {}".format(self.name, self.age, self.category)
